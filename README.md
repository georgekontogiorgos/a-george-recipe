# a-george conda recipe

Home: "https://gitlab.esss.lu.se/nice/conda-bot-testbed/testbed-modules/a-george"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS a-george module
